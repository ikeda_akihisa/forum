package com.example.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Reply;
import com.example.demo.entity.Report;
import com.example.demo.service.ReplyService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;

	@Autowired
	ReplyService replyService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top(@RequestParam(name="start", required=false) String start,
			@RequestParam(name="end", required=false) String end) {
		ModelAndView mav = new ModelAndView();
		// 開始時刻と終了時刻の間の投稿を全件取得
		List<Report> contentData = reportService.findAllReportByDateAndOrderByDate(start, end);
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("start", start);
		mav.addObject("end", end);
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集するidより投稿内容を取得
		Report report = reportService.findOneReport(id);
		// 画面遷移先を指定
		mav.setViewName("/edit");
		// 入力されたentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// コメント内容表示画面
	@GetMapping("/comment/{id}")
	public ModelAndView newComment(@PathVariable("id") Integer reportId) {
		ModelAndView mav = new ModelAndView();
		// 投稿を取得する
		Report report = reportService.findOneReport(reportId);
		// 投稿に対してのコメントを取得する
		List<Reply> commentData = replyService.findByReplyId(reportId);
		// 空のコメントを作成し、投稿番号を入れる
		Reply reply = new Reply();
		reply.setReportId(reportId);
		// 画面遷移先を指定
		mav.setViewName("/comment");
		// データオブジェクトを保管
		mav.addObject("content", report);
		mav.addObject("comments", commentData);
		mav.addObject("formModel", reply);
		return mav;
	}

	// コメント編集画面
	@GetMapping("/comment/edit/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集するidよりコメント内容を取得
		Reply reply = replyService.findOneReply(id);
		// 画面遷移先を指定
		mav.setViewName("/commentEdit");
		// 入力されたentityを保管
		mav.addObject("formModel", reply);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント投稿処理
	@PostMapping("/comment/add/{id}")
	public ModelAndView addComment(@ModelAttribute("formModel") Reply reply,
			@PathVariable Integer id) {
		// コメントに投稿IDをセット
		reply.setReportId(id);
		// コメントをテーブルに格納
		replyService.saveReply(reply);
		// 投稿idより投稿を取得
		Report report = reportService.findOneReport(reply.getReportId());
		// 投稿の更新日をセット
		report.setUpdatedDate(new Date());
		// 投稿を更新
		reportService.saveReport(report);
		// コメント表示画面へリダイレクト
		return new ModelAndView("redirect:/comment/" + id);
	}

	// 投稿削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		// 投稿をテーブルから削除
		reportService.deleteReport(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
	// 返信削除処理
		@DeleteMapping("/comment/delete/{id}")
		public ModelAndView deleteComment(@PathVariable Integer id) {
			// 返信のidより返信を取得
			Reply reply = replyService.findOneReply(id);
			// 返信をテーブルから削除
			replyService.deleteReply(id);
			// 返信内容表示画面へリダイレクト
			return new ModelAndView("redirect:/comment/" + reply.getReportId());
		}

	// 投稿編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id,
			@ModelAttribute("formModel") Report report) {
		// 入力された新しい投稿のidを編集したい投稿のものにセット
		report.setId(id);
		// 更新日をセット
		report.setUpdatedDate(new Date());
		// 投稿を更新
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}
	// コメント編集処理
	@PutMapping("/comment/update/{id}")
	public ModelAndView updateComment(@PathVariable Integer id,
			@ModelAttribute("comment") String comment) {
		// 返信のidより返信を取得
		Reply reply = replyService.findOneReply(id);
		// 返信に紐づいている投稿idより投稿を取得
		Report report = reportService.findOneReport(reply.getReportId());
		// 更新日をセット
		reply.setUpdatedDate(new Date());
		report.setUpdatedDate(new Date());
		// 返信のコメントを変更
		reply.setComment(comment);
		// 返信を更新
		replyService.saveReply(reply);
		// 投稿を更新
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/comment/" + reply.getReportId());
	}
}
