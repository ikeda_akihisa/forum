package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Reply;

@Repository
public interface ReplyRepository extends JpaRepository<Reply, Integer> {
	@Query(value = "select t from Reply t where t.reportId = :reportId")
	List<Reply> findByReportId(Integer reportId);
}