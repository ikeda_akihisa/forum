package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {
	@Query(value = "select t from Report t where created_date between :start and :end order by updated_date desc")
	List<Report> findAllByDateAndOrderByDate(Date start, Date end);
}