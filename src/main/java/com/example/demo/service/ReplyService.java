package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Reply;
import com.example.demo.repository.ReplyRepository;

@Service
public class ReplyService {
	@Autowired
	ReplyRepository replyRepository;

	// 投稿に対するコメントを取得
	public List<Reply> findByReplyId(Integer replyId) {
		return replyRepository.findByReportId(replyId);
	}
//	// コメントレコード全件取得
//	public List<Reply> findAllReply() {
//		return replyRepository.findAll();
//	}

	// コメントレコード1件取得
	public Reply findOneReply(Integer id) {
		Reply reply = (Reply) replyRepository.findById(id).orElse(null);
		return reply;
	}
	// コメントレコード追加
	public void saveReply(Reply reply) {
		replyRepository.save(reply);
	}
	// コメントレコード削除
	public void deleteReply(int id) {
		replyRepository.deleteById(id);
	}

}
