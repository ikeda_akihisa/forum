package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// 投稿レコード全件取得
	public List<Report> findAllReport() {
		return reportRepository.findAll();
	}

	// 投稿レコードを日付で検索
	SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public List<Report> findAllReportByDateAndOrderByDate(String start, String end) {
		// startに入力があれば同日00:00:00に変更する
		if (!StringUtils.isEmpty(start)) {
			start += " 00:00:00";
		} else {
			start = "2022-02-01 00:00:00"; // 入力がなければ初期設定時刻
		}

		// endに入力があれば同日23:59:59に変更する
		if (!StringUtils.isBlank(end)) {
			end += " 23:59:59";
		} else {
			end = f.format(new Date()); // 入力がなければ現在時刻
		}

		// 開始時刻と終了時刻をDate型に変換
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = f.parse(start);
			endDate = f.parse(end);

		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		return reportRepository.findAllByDateAndOrderByDate(startDate, endDate);
	}

	// 投稿レコード1件取得
	public Report findOneReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	// 投稿レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// 投稿レコード削除
	public void deleteReport(int id) {
		reportRepository.deleteById(id);
	}

}
